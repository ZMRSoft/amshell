﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using ZooKeeperNet;

namespace AppMonitor.Plugin.Zookeeper
{
    public class ZookeeperClient
    {
        private static void test() {
            //创建一个Zookeeper实例，第一个参数为目标服务器地址和端口，第二个参数为Session超时时间，第三个为节点变化时的回调方法 
            using (ZooKeeper zk = new ZooKeeper("192.168.20.127:2181", new TimeSpan(0, 0, 0, 50000), new ZKWatcher()))
            {
               /* var stat = zk.Exists("/root", true);
                ////创建一个节点root，数据是mydata,不进行ACL权限控制，节点为永久性的(即客户端shutdown了也不会消失) 
                //zk.Create("/root", "mydata".GetBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.Persistent);

                //在root下面创建一个childone znode,数据为childone,不进行ACL权限控制，节点为永久性的 
                zk.Create("/root/childone", "childone".GetBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.Persistent);
                //取得/root节点下的子节点名称,返回List<String> 
                zk.GetChildren("/root", true);
                //取得/root/childone节点下的数据,返回byte[] 
                zk.GetData("/root/childone", true, null);

                //修改节点/root/childone下的数据，第三个参数为版本，如果是-1，那会无视被修改的数据版本，直接改掉
                zk.SetData("/root/childone", "childonemodify".GetBytes(), -1);
                //删除/root/childone这个节点，第二个参数为版本，－1的话直接删除，无视版本 
                zk.Delete("/root/childone", -1);*/
                byte[] b = zk.GetData("/dubbo", false, null);
                Console.WriteLine("====>" + b);
                //IEnumerable<String> s = zk.GetChildren("/", false);
                //Console.WriteLine("====>" + s);

            }
        }

        public static Task<string> Run4Cmd(string host, int port, string cmd)
        {
            //四字命令  
            var command = Encoding.ASCII.GetBytes(cmd);
            //打开原生客户端  
            var tcp = OpenNativeClient(host, port);
            var stream = tcp.GetStream();
            stream.Write(command, 0, command.Length);

            var data = new byte[1024 * 50];
            var reading = Task<int>.Factory.FromAsync(stream.BeginRead, stream.EndRead, data, 0, data.Length, null);

            return reading.ContinueWith(length =>
            {
                tcp.Close();
                if (length.Result < data.Length)
                {
                    Array.Resize(ref data, length.Result);
                }
                return new UTF8Encoding().GetString(data);
            });  
        }

        public static TcpClient OpenNativeClient(string host, int port) {
            return new TcpClient(host, port);
        }

    }
}
