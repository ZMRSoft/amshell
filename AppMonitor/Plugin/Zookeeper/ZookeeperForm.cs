﻿using AppMonitor.Bex;
using AppMonitor.Froms;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;

namespace AppMonitor.Plugin.Zookeeper
{
    public partial class ZookeeperForm : CCWin.Skin_Metro
    {
        private static ILog logger = LogManager.GetLogger("LogFileAppender");
        private CZookeeper zook;
        private List<string> nodes;

        Thread t ;

        public ZookeeperForm(CZookeeper zook, List<string> nodes)
        {
            InitializeComponent();
            this.zook = zook;
            this.nodes = nodes;
            SkinUtil.SetFormSkin(this);
        }

        private void ZookeeperForm_Load(object sender, EventArgs e)
        {
            t = new Thread((ThreadStart)delegate()
            {
                loadNodeDatas();
            });
            t.Start();
        }

        public void loadNodeDatas()
        {
            treeView1.BeginInvoke((MethodInvoker)delegate()
            {
                TreeNode node = new TreeNode();
                node.Name = "dubbo";
                node.Text = "dubbo";
                treeView1.Nodes.Add(node);

                loadChildNode(node, nodes, "/dubbo/");

                t.Abort();
            });
        }

        private void loadChildNode(TreeNode node, IEnumerable<string> list, string path)
        {
            TreeNode subNode = null;
            foreach (string c in list)
            {
                subNode = new TreeNode();
                subNode.Name = c;
                subNode.Text = c;
                node.Nodes.Add(subNode);
                try
                {
                    TreeNode consumer = new TreeNode("consumers");
                    consumer.Name = path + c + "/consumers";
                    subNode.Nodes.Add(consumer);

                    

                    TreeNode provider = new TreeNode("providers");
                    provider.Name = path + c + "/providers";
                    subNode.Nodes.Add(provider);                    
                }
                catch (Exception ex)
                {
                    logger.Error("load [" + path + "] nodes Error:" + ex.Message, ex);
                }
            }
        }

        private void cmd4run(object sender, EventArgs e)
        {
            string cmd = ((Button)sender).Text;
            string hp = zook.getHostPort();
            string[] arr = hp.Split(':');
            int port = Convert.ToInt32(arr[1]);
            string result = ZookeeperClient.Run4Cmd(arr[0], port, cmd).Result;
            cmdViewer.Text = result;
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode node = e.Node;
            string name = node.Name;
            if (name.EndsWith("consumers") || name.EndsWith("providers"))
            {
                try
                {
                    IEnumerable<string> list2 = zook.GetNodeChildren(name, false);
                    if (null != list2)
                    {
                        node.Nodes.Clear();
                        foreach (string c2 in list2)
                        {
                            node.Nodes.Add(new TreeNode(HttpUtility.UrlDecode(c2)));
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("load [" + name + "] nodes Error:" + ex.Message, ex);
                }
            }
        }


    }
}
